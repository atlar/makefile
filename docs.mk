# include Makefile for docs (RTD) related targets and variables

# do not declare targets if help had been invoked
ifneq (long-help,$(firstword $(MAKECMDGOALS)))
ifneq (help,$(firstword $(MAKECMDGOALS)))

SHELL := /usr/bin/env bash
# Minimal makefile for Sphinx documentation
#

# You can set these variables from the command line.
DOCS_SPHINXOPTS    ?= ## docs Sphinx opts
DOCS_SPHINXBUILD   ?= python3 -m sphinx## Docs Sphinx build command
DOCS_SOURCEDIR     ?= docs/src## docs sphinx source directory
DOCS_BUILDDIR      ?= docs/build## docs sphinx build directory

DOCS_AUTHOR_NAME ?= Atlar Innovation
DOCS_PROJECT_NAME ?= $(shell basename $(CURDIR))
DOCS_FORMAT ?= html

ifeq (docs-build,$(firstword $(MAKECMDGOALS)))
  # use the rest as arguments for "docs-build"
  DOCS_TARGET_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  # ...and turn them into do-nothing targets
  $(eval $(DOCS_TARGET_ARGS):;@:)
endif

ifeq (docs-help,$(firstword $(MAKECMDGOALS)))
  # use the rest as arguments for "docs-help"
  DOCS_TARGET_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  # ...and turn them into do-nothing targets
  $(eval $(DOCS_TARGET_ARGS):;@:)
endif

# Put it first so that "make" without argument is like "make help".
docs-help:  ## help for docs
	@$(DOCS_SPHINXBUILD) -M help $(DOCS_SOURCEDIR) $(DOCS_BUILDDIR) $(DOCS_SPHINXOPTS) $(DOCS_TARGET_ARGS)

docs-pre-build:

docs-post-build:

# Catch-all target: route all unknown targets to Sphinx using the new
# "make mode" option.  $(O) is meant as a shortcut for $(SPHINXOPTS).
docs-do-build:
	mkdir -p docs/build
	$(DOCS_SPHINXBUILD) -M $(DOCS_TARGET_ARGS) $(DOCS_SOURCEDIR) $(DOCS_BUILDDIR) $(DOCS_SPHINXOPTS)

## TARGET: docs-build
## SYNOPSIS: make docs-build <sphinx command such as html|help|latex|clean|...>
## HOOKS: docs-pre-build, docs-post-build
## VARS:
##       DOCS_SOURCEDIR=<docs source directory> - default ./docs/src
##       DOCS_BUILDDIR=<docs build directory> - default ./docs/build
##       DOCS_SPHINXOPTS=<additional command line options for Sphinx>
##
##  Build the RST documentation in the ./docs directory.

docs-build: docs-pre-build docs-do-build docs-post-build  ## Build docs - must pass sub command

## TARGET: docs-generate
## SYNOPSIS: make docs-generate
## HOOKS:
## VARS:
##       DOCS_AUTHOR_NAME=<docs author> - default Atlar Innovation
##       DOCS_PROJECT_NAME=<docs build directory> - default ./docs/build
##       DOCS_FORMAT=<final docs format> - default html
##
##  Build the RST documentation in the ./docs directory.

docs-generate:
	@rm -Rf docs
	@mkdir -p $(DOCS_SOURCEDIR)
	cd $(DOCS_SOURCEDIR) && \
	sphinx-quickstart -q --no-sep -p $(DOCS_PROJECT_NAME) -a "$(DOCS_AUTHOR_NAME)" --ext-autodoc --ext-todo --ext-viewcode --makefile --no-batchfile && \
	sphinx-apidoc -o . ../../src
	@sed '8i\import os\nimport sys\nsys.path.insert(0, os.path.abspath('"'"'../src'"'"'))' -i "$(DOCS_SOURCEDIR)/conf.py"
	@sed -i 's/alabaster/sphinx_rtd_theme/' "$(DOCS_SOURCEDIR)/conf.py"
	@sed '13i\   modules' -i "$(DOCS_SOURCEDIR)/index.rst"

.PHONY: docs-help docs-pre-build docs-do-build docs-post-build docs-build docs-pre-generate docs-do-generate docs-post-generate docs-generate

# end of switch to suppress targets for help
endif
endif
